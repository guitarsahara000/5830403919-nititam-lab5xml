<?php    
    /* create a dom document with encoding utf8 */
    $domtree = new DOMDocument('1.0', 'UTF-8');

    /* create the root element of the xml tree */
    $xmlRoot = $domtree->createElement("xml");
    /* append it to the document created */
    $domtree->appendChild($xmlRoot);

    $attr= $domtree->createAttribute("id");
    $attr->value="5830403919";    
    $xmlRoot->appendChild($attr);

    $nodemajor = $domtree->createElement("major");
    $xmlRoot->appendChild($nodemajor);
    $nodemajor->appendChild($domtree->createTextNode("Computer Engineer"));

    $nodearea = $domtree->createElement("area");
    $nodearea->appendChild($domtree->createTextNode("Software"));
    $nodemajor->appendChild($nodearea);

    $nodename = $domtree->createElement("name");
    $nodename->appendChild($domtree->createTextNode("Nititam Bungthong"));
    $xmlRoot->appendChild($nodename);
   

    $domtree->save("profile.xml");
    echo "finised";
?>