<?php
$xmlDoc = new DOMDocument();
$xmlDoc->load("nation.xml");
doSomething($xmlDoc);

function doSomething($xmlDoc) {	
     $nodeList = $xmlDoc->childNodes;
 
   foreach($xmlDoc->childNodes as $currentChildNode )
   {
       $countChild=0;       
   	  
   	  echo "===========================================================<br>";
      
      echo "Node type is ", typeIs($currentChildNode->nodeType);
      echo " name is ", $currentChildNode->nodeName;
      echo " value is ", $currentChildNode->nodeValue, "<br>";
           

      if($currentChildNode->hasChildNodes()){
      foreach($currentChildNode->childNodes as $cnt){
       $countChild++;

      }
      echo "node ",$currentChildNode->nodeName," has ", $countChild, " children <br>"; 
      doSomething($currentChildNode);
      }    
   }



} 

function typeIs($type){
	if($type == 1) return "ELEMENT_NODE";
	if($type == 2) return "ATTRIBUTE_NODE";
	if($type == 3) return "TEXT_NODE";
	if($type == 4) return "CDATA_SECTION_NODE";
	if($type == 5) return "ENTITY_REFERENCE_NODE";
	if($type == 6) return "ENTITY_NODE";
	if($type == 7) return "PROCESSING_INSTRUCTION_NODE";
	if($type == 8) return "COMMENT_NODE";
	if($type == 9) return "DOCUMENT_NODE";
	if($type == 10) return "DOCUMENT_TYPE_NODE";
	if($type == 11) return "DOCUMENT_FRAGMENT_NODE";
	if($type == 12) return "NOTATION_NODE";

}
?> 